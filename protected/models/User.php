<?php

/**
 * Модель таблицы "User"
 *
 * Доступные столбцы из таблицы "User":
 * @property integer $id
 * @property string $created_date
 * @property string $username
 * @property integer $icq
 * @property string $jabber
 * @property string $email
 * @property integer $balance
 * @property integer $group
 * @property integer $security_key
 * @property integer $authoritet
 * @property integer $rank
 */
class User extends CActiveRecord
{
    public $password2;
    public $verifyCode;
    public $securityKey;
   
     
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    
    public function tableName()
    {
        return 'user';
    }

    /**
     * @return array правила валидации
     */
    public function rules()
    {
        return array(
            array('username, password, password2', 'required'),
            array('icq, balance, group, security_key, authoritet, rank', 'numerical', 'integerOnly'=>true),
            array('username, password', 'length', 'max'=>20),
            array('jabber, email', 'length', 'max'=>40),
            array('id, created_date, username, icq, jabber, email, balance, group, security_key, authoritet, rank', 'safe', 'on'=>'search'),
            array('password', 'compare', 'compareAttribute' => 'password2'),
            array('email', 'email'),
            array('username', 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,]+$/u','message' => 'Логин содержит недопустимые символы.'),
            array('verifyCode', 'captcha', 'allowEmpty'=>!extension_loaded('gd'), 'on' => 'registration'),
       );
    }

    /**
     * @return array реляция БД.
     */
    public function relations()
    {

        return array(
        );
    }
    
    public function getSafeAttributes()
         {
         return array('username, password, password2, icq, email, jabber, verifyCode');
         }

    /**
     * @return array именование переменных
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'created_date' => 'Дата регистрации',
            'username' => 'Логин',
            'icq' => 'ICQ',
            'jabber' => 'Jabber',
            'email' => 'E-mail',
            'balance' => 'Баланс',
            'group' => 'Группа',
            'security_key' => 'Security key',
            'authoritet' => 'Авторитет',
            'rank' => 'Рейтинг',
            'password' => 'Пароль',
            'password2' => 'Повторите пароль',
            'verifyCode' => 'Код подтверждения',
        );
    }
    
    
    public function search()
    {

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('created_date',$this->created_date,true);
        $criteria->compare('username',$this->username,true);
        $criteria->compare('icq',$this->icq);
        $criteria->compare('jabber',$this->jabber,true);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('balance',$this->balance);
        $criteria->compare('group',$this->group);
        $criteria->compare('security_key',$this->security_key);
        $criteria->compare('authoritet',$this->authoritet);
        $criteria->compare('rank',$this->rank);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
    
    
    public function beforeSave()
    {
          if(parent::beforeSave()){
          if($this->isNewRecord){
               $this->kryptPassword();
               $this->setSecuritykey();
               }
               return true;
          }
          else{
               return false;
               }
    }
    
     public function validatePassword($password)
    {
        return $this->kryptPassword($password)===$this->password;
    }
    
    public function kryptPassword()
    {
         return $this->password = md5(md5($this->password));
    }
    
    public function setSecuritykey(){

             do{
             $securityKey = mt_rand(0, 9999);
             $result = User::model()->findByAttributes(array('security_key'=>$securityKey));
             } while($result>0);
             return $this->security_key = $securityKey;
   }

    public function balance($username){
        $balance = User::model()->findByAttributes(array('username' => $username));
        return $balance;
    }

}