<?php

/**
 * This is the model class for table "deal".
 *
 * The followings are the available columns in table 'deal':
 * @property integer $id
 * @property string $created_date
 * @property string $title
 * @property integer $amount
 * @property string $user_out
 * @property string $user_in
 * @property integer $userconfirm_out
 * @property integer $userconfirm_in
 * @property integer $status
 * @property string $comment
 */
class Deal extends CActiveRecord
{
    public $verifyCode;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Deal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'deal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, amount, user_in, comment, verifyCode', 'required'),
			array('amount, userconfirm_out, userconfirm_in, status', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>100),
			array('user_out, user_in', 'length', 'max'=>20),
            array('user_in', 'exist', 'attributeName'=>'username', 'className'=>'User', 'message' => 'Вы не можете оформить с данным участником сделку. Таких граждан у нас нет или он забанен!'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, created_date, title, amount, user_out, user_in, userconfirm_out, userconfirm_in, status, comment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'owner' => array(self::BELONGS_TO, 'User', array('user_in'=>'username')),
            'initiator' => array(self::BELONGS_TO, 'User', array('user_out'=>'username')),
		);
	}

    public function allPartners($username){
        $this->getDbCriteria()->mergeWith(array('condition' => 'user_in="'.$username.'"OR user_out="'.$username.'"'));
        return $this;
    }

    public function userInitiator($username){
        $this->getDbCriteria()->mergeWith(array('condition' => 'user_out="'.$username.'"'));
        return $this;
    }

    public function userOwner($username){
        $this->getDbCriteria()->mergeWith(array('condition' => 'user_in="'.$username.'"'));
        return $this;
    }

    public function getSafeAttributes(){
        return array('user_in, amount, title, comment, verifyCode');
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'created_date' => 'Дата создание',
			'title' => 'Название сделки',
			'amount' => 'Сумма',
			'user_out' => 'Инициатор сделки',
			'user_in' => 'Партнер',
			'userconfirm_out' => 'Подтверждение инициатора',
			'userconfirm_in' => 'Подтверждение партнера',
			'status' => 'Статус',
			'comment' => 'Условия',
            'verifyCode' => 'Код подтверждения',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('user_out',$this->user_out,true);
		$criteria->compare('user_in',$this->user_in,true);
		$criteria->compare('userconfirm_out',$this->userconfirm_out);
		$criteria->compare('userconfirm_in',$this->userconfirm_in);
		$criteria->compare('status',$this->status);
		$criteria->compare('comment',$this->comment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));


	}

    public function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
            {
                $this->user_out=Yii::app()->user->name;
            }
            return true;
        }
        else{
            return false;
        }
    }
}