<?
/*
 * Класс контроллера сделки
 */

class DealController extends Controller
{
    private $_id;

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ));
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'list' and 'show' actions
                'actions'=>array('index', 'view'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated users to perform any action
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /*
     * Действие index, оно же создание сделки
     */
    public function actionIndex()
    {
    $model = new Deal();

        if(isset($_POST['Deal'])){
            $model->attributes=$_POST['Deal'];
            if($model->validate()){
                $model->save();
            }
        }
        $this->render('index', array('model'=>$model));
    }

    public function actionViewin()
    {
    $userOwner = new CActiveDataProvider(Deal::model()->userOwner(Yii::app()->user->name));
    $this->render('view', array('dataProvider' => $userOwner));
    }

     public function actionViewout()
    {
    $userInitiator = new CActiveDataProvider(Deal::model()->userInitiator(Yii::app()->user->name));
    $this->render('view', array('dataProvider' => $userInitiator));
    }

    public function actionDetail()
    {
        if(isset($_GET['id']))
        {
          $dealDetail = Deal::model()->findByPk($_GET['id']);
          if(($dealDetail['user_in'] or $dealDetail['user_out']) == Yii::app()->user->name)
          {
          $this->render('detail', array('dealDetail' => $dealDetail));
          }
        }

        throw new CHttpException(1,'Запрашиваемая страница найдена. Но ты долбоеб, меньше играйся с GET.');
    }

}