
<div class="span4" style="margin: 20% 35%;">
     <center><h2>ВХОД</h2></center>
<?php /** @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'verticalForm',
    'htmlOptions'=>array('class'=>'well'),
));?>
<?php echo $form->textFieldRow($model, 'username', array('class'=>'span3')); ?>
<?php echo $form->passwordFieldRow($model, 'password', array('class'=>'span3')); ?> <br>
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit','type'=>'primary','htmlOptions'=>array('style'=>'width:115px;'), 'label'=>'Вход')); ?>
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'link','type'=>'link','url'=>array('site/registration'), 'htmlOptions'=>array('style'=>'margin-left:5px;'), 'label'=>'Регистрация')); ?>
<?php $this->endWidget(); ?>
</div>