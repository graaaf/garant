
<div class="container">
  <div class="hero-unit">
  <h1>Личное дело<span class="label label-success"><abbr title="Данному участнику можно доверять">Подтвержден</abbr></span></h1>
  <p>Твое личное дело, <?=$info->username;?></p>
  <abbr title="Данный индификатор, который вы должны указать на форуме в специально форме">Security Key:</abbr> <?=$info->security_key;?></br>
  <abbr title="Аналог BL в webmoney">Авторитет:<?=$info->authoritet;?></abbr> </br>
    ICQ: <?=$info->icq;?></br>
    Jabber: <?=$info->jabber;?></br>
    Email: <?=$info->email;?>
  </div>

    <div class="alert alert-block">
    <h4>Внимание!</h4>
    Перед сделкой всегда проверяйте <strong>Security ID</strong>, но и это не гарантирует то, что ваш вчерашний партнер стал кидалой.</br>
    Статус <span class="label label-success"><abbr title="Данному участнику можно доверять">Подтвержден</abbr></span> также не дает 100% гарантии о том что у гражданина добрые намерения.
  </div>

</div>