<div class="span4" style="margin: 10% 35%;">
     <center><h2>РЕГИСТРАЦИЯ</h2></center>
<?php /** @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'verticalForm',
    'htmlOptions'=>array('class'=>'well'),
));?>
    <?php echo $form->errorSummary($model); ?>
<?php echo $form->textFieldRow($model, 'username', array('class'=>'span3')); ?>
<?php echo $form->passwordFieldRow($model, 'password', array('class'=>'span3')); ?> <br>
<?php echo $form->passwordFieldRow($model, 'password2', array('class'=>'span3')); ?> <br>
<?php echo $form->textFieldRow($model, 'icq', array('class'=>'span3')); ?>
<?php echo $form->textFieldRow($model, 'jabber', array('class'=>'span3')); ?>
<?php echo $form->textFieldRow($model, 'email', array('class'=>'span3')); ?>
<?php echo $form->CaptchaRow($model, 'verifyCode', array('class'=>'span3')); ?>
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit','type'=>'primary','htmlOptions'=>array('style'=>'width:115px;'), 'label'=>'Регистрация')); ?>
<?php $this->endWidget(); ?>
</div>