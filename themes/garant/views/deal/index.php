<div class="hero-unit" xmlns="http://www.w3.org/1999/html">
       <?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'verticalForm',
        'htmlOptions'=>array('class'=>'well'),
        ));?>
        <?php echo $form->textFieldRow($model, 'title', array('class'=>'span3')); ?><br/>
        <?php echo $form->textFieldRow($model, 'user_in', array('class'=>'span3')); ?><br/>
        <?php echo $form->textAreaRow($model, 'comment', array('class'=>'span8', 'rows'=>10, 'placeholder'=>'Условия сделки заполняйте максимально подробно и точно. В неприятных ситуациях арбитраж рассматривает те условия, которые были оговорены в данной форме.')); ?>
        <?php echo $form->textFieldRow($model, 'amount', array('class'=>'span2')); ?><br/><br/>
        <?php echo $form->CaptchaRow($model, 'verifyCode', array('class'=>'span2')); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit','type'=>'primary','htmlOptions'=>array('style'=>'width:115px;'), 'label'=>'Оформить')); ?>
        <?php $this->endWidget(); ?>
    </div>
    <div class="alert alert-block">
        <h4>Внимание!</h4>
        Перед сделкой всегда проверяйте <strong>Security ID</strong>, но и это не гарантирует то, что ваш вчерашний партнер стал кидалой.</br>
        Статус <span class="label label-success"><abbr title="Данному участнику можно доверять">Подтвержден</abbr></span> также не дает 100% гарантии о том что у гражданина добрые намерения.
    </div>