<div class="row">
	<div class="span8 offset2">
	<?if (Yii::app()->controller->action->id == 'viewin'):?>
		<center><h3>Входящие сделки</h3><center>
            <?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'dataProvider' => $dataProvider,
    'template' => "{items}",
    'columns' => array(
        array('name' => 'created_date', 'header' => 'Дата'),
        array('name'=>'user_out', 'header' => 'Партнер'),
        array('name'=>'title', 'header' => 'Название сделки'),
        array('name'=>'amount', 'header' => 'Сумма'),
        
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{view}',
            'buttons' => array(
                'view' => array('url' => 'Yii::app()->createUrl("deal/detail", $params = array("id" => $data["id"]))'),
                ),
        ),
    ),
));?>
	<?else:?>
		<center><h3>Исходящие сделки</h3><center>		

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'dataProvider' => $dataProvider,
    'template' => "{items}",
    'columns' => array(
        array('name' => 'created_date', 'header' => 'Дата'),
        array('name'=>'user_in', 'header' => 'Партнер'),
        array('name'=>'title', 'header' => 'Название сделки'),
        array('name'=>'amount', 'header' => 'Сумма'),
        
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{view}',
            'buttons' => array(
                'view' => array('url' => 'Yii::app()->createUrl("deal/detail", $params = array("id" => $data["id"]))'),
                ),
        ),
    ),
));?>

	<?endif;?>




	</div>
</div>